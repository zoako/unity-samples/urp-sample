# Universal Pipeline Shader Samples
The [Universal Render Pipeline](https://docs.unity3d.com/Packages/com.unity.render-pipelines.universal@7.2/manual/index.html) has replaced the LWRP.

## TODO
- Upgrade from LWRP [following this guide](https://docs.unity3d.com/Packages/com.unity.render-pipelines.universal@7.2/manual/upgrade-lwrp-to-urp.html)
- Migrate 

## Post-processing
[Docs for post-processing](https://docs.unity3d.com/Packages/com.unity.render-pipelines.lightweight@6.9/manual/integration-with-post-processing.html) in LWRP